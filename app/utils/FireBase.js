import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDG85QBg9t8Pe9R7nbDYvCKP7D_yJ_waS0",
  authDomain: "tenedores-b04f3.firebaseapp.com",
  databaseURL: "https://tenedores-b04f3.firebaseio.com",
  projectId: "tenedores-b04f3",
  storageBucket: "tenedores-b04f3.appspot.com",
  messagingSenderId: "671433946869",
  appId: "1:671433946869:web:c508e97518efdb43ff37bc",
  measurementId: "G-7SRT13HX7B"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
