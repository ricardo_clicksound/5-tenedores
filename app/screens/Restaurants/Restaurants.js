import React from "react";
import { View, Text } from "react-native";
import ActionButton from "react-native-action-button";

export default function Restaurants() {
  return (
    <View>
      <Text>Estamos en restaurantes.</Text>
      <AddRestaurantButton />
    </View>
  );
}

function AddRestaurantButton() {
  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => console.log("Navegando a Add Restaurant")}
    />
    // <Text>Hola</Text>
  );
}
